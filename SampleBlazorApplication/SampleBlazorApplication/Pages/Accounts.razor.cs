﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DevExpress.Blazor;
using Newtonsoft.Json;
using SampleBlazorApplication.Models;

namespace SampleBlazorApplication.Pages
{
    
    
    partial class Accounts
    {
        HttpClient httpClient;
        public Accounts()
        {
            httpClient = new HttpClient();
        }

        
        DxDataGrid<Account> grid;
        IEnumerable<Account> accounts;
        bool popupVisible = false;
        bool isSuccess = false;
        bool isfailure = false;

        protected override async Task OnInitializedAsync()
        {

            var responseMessage = httpClient.GetAsync("https://localhost:44307/api/Accounts");
            var data = responseMessage.Result.Content.ReadAsStringAsync();
            accounts = JsonConvert.DeserializeObject<List<Account>>(data.Result);
        }

        async void OnRowInserting(Dictionary<string, object> newValues)
        {
            Account account = new Account();
            //SetNewValues(account, newValues);
          
            await InvokeAsync(StateHasChanged);
        }

        async void OnRowRemoving(PatientModel dataItem)
        {

            await InvokeAsync(StateHasChanged);
        }

        async void OnRowUpdating(PatientModel dataItem, Dictionary<string, object> newValues)
        {
            //SetNewValues(dataItem, newValues);

            await InvokeAsync(StateHasChanged);
        }

        FormEditContext EditContext = null;
        void OnRowEditStarting(Account account)
        {
            //EditContext = new FormEditContext(account);
            //EditContext.StateHasChanged += () => { InvokeAsync(StateHasChanged); };
        }
    }
}
