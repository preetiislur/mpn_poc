﻿using DevExpress.Blazor;
using SampleBlazorApplication.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SampleBlazorApplication.Pages
{
    class FormEditContext
    {
        public FormEditContext(PatientModel dataItem)
        {
            DataItem = dataItem;
            if (DataItem == null)
            {
                DataItem = new PatientModel();
                IsNewRow = true;
            }
            FirstName = DataItem.FirstName;
            LastName = DataItem.LastName;
            PhoneNumber = DataItem.PhoneNumber;
            Dob = DataItem.Dob;
        }

        public PatientModel DataItem { get; set; }
        public bool IsNewRow { get; set; }

        [Required]
        [StringLength(maximumLength: 16, MinimumLength = 4,
        ErrorMessage = "The description should be 4 to 16 characters.")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(maximumLength: 16, MinimumLength = 4,
       ErrorMessage = "The description should be 4 to 16 characters.")]
        public string LastName { get; set; }


        [Required]
        
        [Phone]
        public string PhoneNumber { get; set; }

        public DateTime Dob { get; set; }
        //string region;
        //[Required]
        //public string Region
        //{
        //    get => region;
        //    set
        //    {
        //        region = value;
        //        City = null;
        //        OfficeLocations = VacancyRepository.GetOfficeLocationsByRegion(value).Select(x => x.City);
        //        StateHasChanged?.Invoke();
        //    }
        //}



        public Action StateHasChanged { get; set; }
    }

    
    partial class PatientGrid
    {
        private List<PatientModel> patients;
        DxDataGrid<PatientModel> grid;
        IEnumerable<PatientModel> patientmodels;
        bool popupVisible = false;
        bool isSuccess = false;
        bool isfailure = false;

        protected override async Task OnInitializedAsync()
        {

            patientmodels = await PatientService.GetPatientsAsync();
        }

        void OnEditButtonClick()
        {

        }

        void OnAddButtonClick()
        {

        }
        void OnDeleteButtonClick()
        {
            // your code
        }

        async void OnRowInserting(Dictionary<string, object> newValues)
        {
            PatientModel newpatient = new PatientModel();
            SetNewValues(newpatient, newValues);
            var result = PatientService.CreatePatient(newpatient);
            await InvokeAsync(StateHasChanged);
        }

        async void OnRowRemoving(PatientModel dataItem)
        {

            await InvokeAsync(StateHasChanged);
        }

        async void OnRowUpdating(PatientModel dataItem, Dictionary<string, object> newValues)
        {
            SetNewValues(dataItem, newValues);

            await InvokeAsync(StateHasChanged);
        }

        private void SetNewValues(PatientModel dataItem, Dictionary<string, object> newValues)
        {
            foreach (var field in newValues.Keys)
            {
                switch (field)
                {
                    case nameof(PatientModel.FirstName):
                        dataItem.FirstName = (string)newValues[field];
                        break;
                    case nameof(PatientModel.LastName):
                        dataItem.LastName = (string)newValues[field];
                        break;
                    case nameof(PatientModel.PhoneNumber):
                        dataItem.PhoneNumber = (string)newValues[field];
                        break;
                    case nameof(PatientModel.Dob):
                        dataItem.Dob = (DateTime)newValues[field];
                        break;
                }
            }
        }

        FormEditContext EditContext = null;
        void OnRowEditStarting(PatientModel patient)
        {
            EditContext = new FormEditContext(patient);
            EditContext.StateHasChanged += () => { InvokeAsync(StateHasChanged); };
        }
        void OnCancelButtonClick()
        {
            EditContext = null;
            grid.CancelRowEdit();
        }
        void HandleValidSubmit()
        {
            EditContext.DataItem.FirstName = EditContext.FirstName;
            EditContext.DataItem.LastName = EditContext.LastName;
            EditContext.DataItem.PhoneNumber = EditContext.PhoneNumber;
            EditContext.DataItem.Dob = EditContext.Dob;
            if (EditContext.IsNewRow)
            {
                patientmodels = (new PatientModel[] { EditContext.DataItem }).Concat(patientmodels);
                var result = PatientService.CreatePatient(EditContext.DataItem);
                if (result == true)
                {
                    popupVisible = true;
                    isSuccess = true;
                }
                else
                {
                    popupVisible = true;
                    isfailure = true;
                }
            }

            EditContext = null;
            grid.CancelRowEdit();
        }

        //void OnRowRemoving(PatientModel model)
        //{
        //    patientmodels = patientmodels.Where(v => v != model).ToArray();
        //    InvokeAsync(StateHasChanged);
        //}

    }
}
