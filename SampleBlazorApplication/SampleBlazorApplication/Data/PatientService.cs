﻿using Newtonsoft.Json;
using SampleBlazorApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SampleBlazorApplication.Data
{
    public class PatientService
    {
        HttpClient httpClient;
        public PatientService()
        {
             httpClient = new HttpClient();
        }

        public Task<List<PatientModel>> GetPatientsAsync()
        {
            var responseMessage =  httpClient.GetAsync("https://localhost:44307/api/Patients");              
             var data =  responseMessage.Result.Content.ReadAsStringAsync();
                var list=  JsonConvert.DeserializeObject<List<PatientModel>>(data.Result);
            return Task.FromResult(list);         
              
        }


        public bool CreatePatient(PatientModel patient)
        {
            patient.PatientId = Guid.NewGuid();
            var json = JsonConvert.SerializeObject(patient);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = httpClient.PostAsync("https://localhost:44307/api/Patients", data).Result;
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
            
    }
}
