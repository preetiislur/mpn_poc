﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleBlazorApplication.Helper
{
    public enum ToastLevel
    {
        Info,
        Success,
        Warning,
        Error
    }
}
