﻿using System;
using System.Collections.Generic;

namespace ClinicalApplication.Models
{
    public partial class Account
    {
        public DateTime? ServiceDate { get; set; }
        public double? Amount { get; set; }
        public double? DeductibleApplied { get; set; }
        public double? PatientPaid { get; set; }
        public double? PayerPaid { get; set; }
        public double? Balance { get; set; }
        public double? DoctorId { get; set; }
        public double? OfficeId { get; set; }
        public double? BillId { get; set; }
        public double? EntryType { get; set; }
        public double? CptCode { get; set; }
    }
}
