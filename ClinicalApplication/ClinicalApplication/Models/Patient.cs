﻿using System;
using System.Collections.Generic;

namespace ClinicalApplication.Models
{
    public partial class Patient
    {
        public Guid PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Dob { get; set; }
        public string PhoneNumber { get; set; }
    }
}
