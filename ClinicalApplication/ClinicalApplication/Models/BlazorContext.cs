﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ClinicalApplication.Models
{
    public partial class BlazorContext : DbContext
    {
        public BlazorContext()
        {
        }

        public BlazorContext(DbContextOptions<BlazorContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<EntryType> EntryType { get; set; }
        public virtual DbSet<Patient> Patient { get; set; }
        public virtual DbSet<Sheet3> Sheet3 { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Account$");

                entity.Property(e => e.Amount).HasColumnName("  Amount");

                entity.Property(e => e.Balance).HasColumnName("  Balance");

                entity.Property(e => e.BillId).HasColumnName("  BillId");

                entity.Property(e => e.CptCode).HasColumnName("  CptCode");

                entity.Property(e => e.DeductibleApplied).HasColumnName("  DeductibleApplied");

                entity.Property(e => e.DoctorId).HasColumnName("  DoctorId");

                entity.Property(e => e.EntryType).HasColumnName("  EntryType");

                entity.Property(e => e.OfficeId).HasColumnName("  OfficeId");

                entity.Property(e => e.PatientPaid).HasColumnName("  PatientPaid");

                entity.Property(e => e.PayerPaid).HasColumnName("  PayerPaid");

                entity.Property(e => e.ServiceDate)
                    .HasColumnName("  ServiceDate")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<EntryType>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("'Entry Type$'");

                entity.Property(e => e.EntryType1)
                    .HasColumnName("EntryType")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Patient>(entity =>
            {
                entity.Property(e => e.PatientId).ValueGeneratedNever();

                entity.Property(e => e.Dob).HasColumnType("datetime");

                entity.Property(e => e.FirstName).IsRequired();

                entity.Property(e => e.LastName).IsRequired();
            });

            modelBuilder.Entity<Sheet3>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Sheet3$");

                entity.Property(e => e.F1).HasMaxLength(255);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
